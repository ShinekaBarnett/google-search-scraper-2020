function initAutocompleteRegion() {
    const input = document.getElementById("region");
    const searchBox = new google.maps.places.SearchBox(input);
    searchBox.addListener("places_changed", () => {
      const places = searchBox.getPlaces();
      if (places.length == 0) {
        return;
      }
    });
}


function initAutocompleteLanguage() {
  const input = document.getElementById("language");
  const searchBoxL = new google.maps.language.SearchBox(input);
  searchBoxL.addListener("places_changed", () => {
    const language = searchBoxL.getPlaces();
    if (language.length == 0) {
      return;
    }
  });
}